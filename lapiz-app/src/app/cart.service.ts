// En cart.service.ts
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
providedIn: 'root'
})
export class CartService {
// Array que almacena temporalmente los productos seleccionados
private temporaryCartItems: any[] = [];
private finalCartItems: any[] = [];
private cartItemCount = new BehaviorSubject<number>(0);
currentCartItemCount = this.cartItemCount.asObservable();
private apiUrl = 'http://localhost:8080'; // URL del backend

constructor(private http: HttpClient) { }

// Método para agregar un producto al carrito temporal
addToTemporaryCart(item: any) {
const existingItem = this.temporaryCartItems.find((cartItem) => cartItem.productId === item.productId);

    if (existingItem) {
      console.log('El producto ya está en el carrito temporal.');
      // Puedes mostrar un mensaje o lanzar una alerta aquí
    } else {
      console.log('Añadiendo al carrito temporal:', item);
      this.temporaryCartItems.push(item);
    // Incrementa el contador de elementos en el carrito
    this.cartItemCount.next(this.temporaryCartItems.length);
    }
  }

  getTemporaryCartItems() {
    console.log('Obteniendo elementos del carrito temporal:', this.temporaryCartItems);
    return this.temporaryCartItems;
  }

  clearTemporaryCart() {
    console.log('Limpiando el carrito temporal');
    this.temporaryCartItems = [];
    this.cartItemCount.next(0);
  }

  updateTemporaryCart(items: any[]) {
    this.temporaryCartItems = items;
  }

  addToCart(item: any) {
    // Agrega el producto al carrito final
    console.log('Añadiendo al carrito final:', item);
  }


  // Método para actualizar el contador del carrito
  updateCartItemCount(count: number) {
    this.cartItemCount.next(count);
  }

  guardarCompra(compra: any): Observable<any> {
    const url = `${this.apiUrl}/carts`; // Endpoint para guardar compras en tu backend
    // Realiza una solicitud POST al backend con los datos de la compra
    return this.http.post<any>(url, compra);
  }

  guardarDetallesCompra(detallesCompra: any): Observable<any> {
    const url = `${this.apiUrl}/transaction`; // Endpoint para guardar detalles de la compra
    // Realiza una solicitud POST al backend con los detalles de la compra
    return this.http.post<any>(url, detallesCompra);
  }


}
