import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "./product";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
 private baseUrl = "http://localhost:8080/products"; // usar el puerto 5000"http://localhost:5000";
 constructor(private httpClient: HttpClient) { }
  //este metodo permite obtener los productos
  getProducts():Observable<Product[]> {
    return this.httpClient.get<Product[]>(`${this.baseUrl}`);
  }
  getProductById(id: number): Observable<Product> {
    const url = `${this.baseUrl}/${id}`;
    return this.httpClient.get<Product>(url);
}
  /*createProduct(formData: FormData):Observable<Object>{
    return this.httpClient.post(`${this.baseUrl}`,formData);
  }*/
  filterByKeyword(keyword:string): Observable<Product[]> {
    const url = `${this.baseUrl}/filterByKeyword?name=${keyword}`;
    return this.httpClient.get<Product[]>(url);
  }
  filterByTypeGem(keyword:string): Observable<Product[]> {
    const url = `${this.baseUrl}/filter?typeGem=${keyword}`;
    return this.httpClient.get<Product[]>(url);
  }
  filterProductsByPriceRange(minPrice: number, maxPrice: number): Observable<Product[]> {
    const url = `${this.baseUrl}/filterByPrice?minPrice=${minPrice}&maxPrice=${maxPrice}`;
    return this.httpClient.get<Product[]>(url);
  }

    public getAllProductIds(): Observable<number[]> {
        const url = `${this.baseUrl}/ids`;
        return this.httpClient.get<number[]>(url);
    }

  createProduct(formData: FormData): Observable<Product> {
    return this.httpClient.post<Product>(`${this.baseUrl}`, formData);
  }

  updateProduct(id: number, product: Product) {
    return this.httpClient.put(`${this.baseUrl}/${id}`, product);
  }


  updateProductByGemName(gemName: string, updatedProduct: Product): Observable<Product> {
    const url = `${this.baseUrl}/${gemName}`;
    return this.httpClient.put<Product>(url, updatedProduct);
  }

  updateProductById(id: number, updatedProduct: Product): Observable<Product> {
    const url = `${this.baseUrl}/${id}`;
    return this.httpClient.put<Product>(url, updatedProduct);
  }

    deleteProductById(id: number): Observable<any> {
    const url = `${this.baseUrl}/${id}`;
    return this.httpClient.delete(url);
  }

}
