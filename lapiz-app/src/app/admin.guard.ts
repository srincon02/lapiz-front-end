
import { Injectable, inject } from '@angular/core';
import { CanActivateFn, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { LoginService } from './services/auth/login.service';
import { jwtDecode } from 'jwt-decode';
import { Observable } from 'rxjs';

interface DecodedToken {
  role: string;
  // incluye aquí otros campos que puedas tener en tu token
}

export const adminGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree => {
  const loginService = inject(LoginService);
  const userToken = loginService.userToken;
  const decodedToken = jwtDecode<DecodedToken>(userToken.toString());
  console.log("role:", decodedToken.role);
  
  if (decodedToken.role === 'ADMIN') {
    console.log(decodedToken.role, "intentomil")
    return true;
  } else {
    const router = inject(Router);
    console.log("acceso denegado")
    return router.createUrlTree(['/ruta-de-acceso-denegado']);
  }
};
