export class Product {
    id!: number;
    gemName!: string;
    description!: string;
    gemImage!: string;
    typeGem!: string;
    gemPrice!: number;
    unitsAvailable!: number;
    state!: number;
  }
