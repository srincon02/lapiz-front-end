import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ListOfProductsComponent } from './components/list-of-products/list-of-products.component'
import { ProductService } from './product.service';
import {DetailsProductComponent} from "./components/details-product/details-product.component";
import { RegisterComponent } from './components/register/register.component';
import { CreateProductComponent } from './components/create-product/create-product.component';
import {CookieService } from 'ngx-cookie-service';
import { HomeComponent } from './components/home/home.component';
import {UserService} from "./user.service";
import {TranslateLoader, TranslateModule, TranslateService} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { JwtInterceptorService } from './services/auth/jwt-interceptor.service';
import { ErrorInterceptorService } from './services/auth/error-interceptor.service';
import {switchComponent} from'./components/switch/switch.component';
import { CartProductComponent } from './components/cart-product/cart-product.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { CartService } from './cart.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    ListOfProductsComponent,
    HomeComponent,
    DetailsProductComponent,
    RegisterComponent,
    CreateProductComponent,
    switchComponent,
    CartProductComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    GoogleMapsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {provide:HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi:true},
    {provide:HTTP_INTERCEPTORS, useClass: ErrorInterceptorService,multi:true }
    ,ProductService, CookieService, UserService,
  TranslateService, CartService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
// AOT compilation support
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}




