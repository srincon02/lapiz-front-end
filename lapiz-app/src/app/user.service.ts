import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { User} from "./user";


@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseUrl = "http://localhost:8080";

  constructor(private httpClient: HttpClient) { }

  registerUser(user: User): Observable<User> {
    return this.httpClient.post<User>(`${this.baseUrl}/auth/register`, user);
  }
}


