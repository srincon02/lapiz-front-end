import { Component } from '@angular/core';
import {Router} from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'lapiz-app';
  constructor(public router: Router,public translate: TranslateService) {
    translate.addLangs(['en', 'es']);
    const lang = translate.getBrowserLang()
    if (lang !== 'es' && lang !== 'en') {
        translate.setDefaultLang('en');
    }
  }
}
