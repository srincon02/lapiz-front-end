import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListOfProductsComponent } from './components/list-of-products/list-of-products.component';
import {HomeComponent} from "./components/home/home.component";
import {DetailsProductComponent} from "./components/details-product/details-product.component";
import {RegisterComponent} from "./components/register/register.component";
import {CreateProductComponent} from "./components/create-product/create-product.component";
import { LoginComponent } from './components/login/login.component';
import { adminGuard } from './admin.guard';
import {CartProductComponent} from "./components/cart-product/cart-product.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [
{path:'home',component:HomeComponent},
{path:'', redirectTo:'/home',pathMatch:'full'},
{path:'products',component:ListOfProductsComponent},
{path:'', redirectTo:'products',pathMatch:'full'},
{path:'products/:searchTerm',component:ListOfProductsComponent},
{path:'details',component:DetailsProductComponent},
{path:'details/:productId',component:DetailsProductComponent},
{path:'login',component:LoginComponent},
{path:'', redirectTo:'/login',pathMatch:'full'},
{path:'register',component:RegisterComponent},
{path:'', redirectTo:'/register',pathMatch:'full'},
//{path:'', redirectTo:'/create-product',pathMatch:'full'}*/
{ path: 'create', component: CreateProductComponent, canActivate: [adminGuard] },
{path:'cart',component:CartProductComponent},
{path:'', redirectTo:'/cart-product',pathMatch:'full'},
{ path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
