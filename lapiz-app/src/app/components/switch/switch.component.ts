import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { ProductService } from 'src/app/product.service';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.css']
})
export class switchComponent {


  constructor(public translate: TranslateService ) { }

  switchLang = (lang:string)=>{
    this.translate.use(lang)
    console.log(this.translate.currentLang)
  }
}

