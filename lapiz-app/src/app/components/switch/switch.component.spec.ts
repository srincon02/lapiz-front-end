import { ComponentFixture, TestBed } from '@angular/core/testing';

import { switchComponent } from './switch.component';

describe('switchComponent', () => {
  let component: switchComponent;
  let fixture: ComponentFixture<switchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [switchComponent]
    });
    fixture = TestBed.createComponent(switchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
