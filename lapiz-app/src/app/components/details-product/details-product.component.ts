import { Component, OnInit } from '@angular/core';
import { Comment } from "../../../comment";

import { ActivatedRoute, Router } from "@angular/router";
import { ProductService } from "../../product.service";
import { Product } from "../../product";
import { CommentService } from "../../../comment.service";
import Swal from 'sweetalert2';
import { CartService } from "../../cart.service";


@Component({
  selector: 'app-details-product',
  templateUrl: './details-product.component.html',
  styleUrls: ['./details-product.component.css']
})
export class DetailsProductComponent implements OnInit {

  product!: Product;
  commentP!: Comment[];
  loading = true;
  commentText!: string;
  name!: string;
  showAlert: boolean = false;
  quantity: number = 1
  quantityError: string = ''; // Variable para el mensaje de error
  // Nuevas propiedades para la paginación
  itemsPerPage: number = 8;
  currentPage: number = 1;
  submitClicked = false;
  comment: Comment = new Comment();


  constructor(private productService: ProductService, private route: ActivatedRoute, private commentService: CommentService, private router: Router, private cartService: CartService) { }

  ngOnInit() {
    const productId = Number(this.route.snapshot.paramMap.get('productId'));
    this.productService.getProductById(productId).subscribe(product => {
      this.product = product;
      this.loading = false;
      console.log(this.product);
    });

    // Cargamos los comentarios al cargar la página
    this.commentService.getReviewsByGemCode(productId).subscribe(comments => {
      // Ordena los comentarios por fecha de forma descendente (del más reciente al más antiguo)
      this.commentP = comments.sort((a, b) => {
        return new Date(b.date_comment).getTime() - new Date(a.date_comment).getTime();
      });
    });
  }

  onSubmit() {
    this.submitClicked = true; // Marca que se hizo clic en el botón de enviar

      if (this.validateForm()) {


          const newComment = new Comment();
    newComment.revDescription = this.commentText;
    newComment.userEmail = this.name;
    newComment.gemCode = this.product;

    console.log('ID del producto:', this.product.id);

    this.commentService.createReview(newComment).subscribe(response => {
      console.log(response);
      this.commentText = '';

      this.showCommentNotification();
      // window.location.reload();

    });
  }
  }

  checkQuantity() {
    const enteredQuantity = parseInt(String(this.quantity), 10);

    if (enteredQuantity > this.product.unitsAvailable) {
      this.quantityError = 'Unidades actualmente no disponibles';
    } else {
      this.quantityError = ''; // Borra el mensaje de error cuando la cantidad es válida
    }
  }



  showCommentNotification() {
    Swal.fire({
      title: 'Nuevo comentario',
      icon: 'success',
      confirmButtonText: 'Registrado',
      confirmButtonColor: '#00473A',
      customClass: {
        popup: 'custom-alert'
      },
      timer: 5000, // Muestra la notificación durante 5000 milisegundos (5 segundos)
      timerProgressBar: true, // Muestra una barra de progreso que indica cuánto tiempo queda antes de que se cierre la notificación
      allowOutsideClick: false // Evita que la notificación se cierre cuando se hace clic fuera de ella
    }).then((result) => {
      // Recarga la página cuando se cierra la alerta, ya sea porque se acabó el tiempo o porque el usuario hizo clic en "Ok"
      window.location.reload();
    });
  }

  // Modificaciones Redyel

  showNotification: boolean = false;

  hideNotificationAfterDelay() {
    setTimeout(() => {
      this.showNotification = false;
    }, 1800); // Cambia 3000 a la cantidad de milisegundos que desees
  }

  addToCart() {
    // Realiza los cálculos necesarios al agregar un producto al carrito temporal
    const total = this.quantity * this.product.gemPrice;
    const vat = 0.19 * total;
    const grandTotal = total + vat;

    // Prepara los datos para el carrito temporal
    const cartItem = {
      productId: this.product.id,
      quantity: this.quantity,
      price: this.product.gemPrice,
      total: total,
      vat: vat,
      grandTotal: grandTotal,
      user: this.name,
      purchaseDate: new Date(), // Puedes ajustar esto según tus necesidades de sesionamiento
      product: this.product  // Asegúrate de incluir el objeto completo del producto
    };

    console.log('Producto agregado al carrito temporal:', cartItem);

    // Verifica si el producto ya está en el carrito temporal
    const existingCartItemIndex = this.cartService.getTemporaryCartItems().findIndex(item => item.productId === this.product.id);

    if (existingCartItemIndex !== -1) {
      // Si el producto ya está en el carrito temporal, actualiza la cantidad al valor actual
      this.cartService.getTemporaryCartItems()[existingCartItemIndex].quantity = this.quantity;
      this.cartService.updateTemporaryCart(this.cartService.getTemporaryCartItems());
    } else {
      // Si no está en el carrito temporal, agrega el producto al carrito temporal
      this.cartService.addToTemporaryCart(cartItem);
    }

    // Reinicia la cantidad después de agregar al carrito
    this.quantity = 1;

    // Agrega el producto al carrito temporal a través del servicio
    this.cartService.addToTemporaryCart(cartItem);
    // Muestra la notificación
    this.showNotification = true;
    // Llama a la función para ocultar la notificación después de cierto tiempo
    this.hideNotificationAfterDelay();
  }



  // Nuevos métodos para la paginación
  get totalPages(): number {
    return Math.ceil(this.commentP.length / this.itemsPerPage);
  }

  get paginatedComments(): Comment[] {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.commentP.slice(startIndex, endIndex);
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  nextPage() {
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
    }
  }

  validateForm(): "" | string | null {
    // Verificar si los campos obligatorios están llenos
    return (
        this.comment.userEmail &&
        this.comment.revDescription
    );
  }

}
