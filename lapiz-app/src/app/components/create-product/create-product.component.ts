import { Component, OnInit } from '@angular/core';
// import { ToastrService } from 'ngx-toastr'; // Importa ToastrService
import { FormGroup } from '@angular/forms'; // Importa FormBuilder y FormGroup
import { ProductService } from '../../product.service';
import { Product } from "../../product";
import Swal from "sweetalert2";

@Component({
selector: 'app-create-product',
templateUrl: './create-product.component.html',
styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent  implements OnInit {
productData!: FormGroup; // Declara un formulario reactivo
product: Product = new Product();
activeTab: 'create' | 'update' | 'delete' = 'create';
successMessageVisible: boolean = false;
submitClicked = false;


productList: Product[] = [];
selectedProductId: number | undefined;

selectedProduct: Product = new Product();
createProduct: Product = new Product(); // Objeto para crear un nuevo producto

selectedProductIdToDelete: number | undefined;

ngOnInit() {
    this.loadProducts();
  }

constructor(
    private productService: ProductService)
    // private toastr: ToastrService)
  {}

  onFileSelected(event: any) {
    const gemImage = event.target.files[0];
    if (gemImage) {
      this.product.gemImage = gemImage;
    }
  }

  onAddProduct() {
    // Verifica si se ha seleccionado una imagen
    if (this.product.gemImage && this.validateForm()) {
      const productData = new FormData();
      productData.append('file', this.product.gemImage);
      productData.append('gemName', this.product.gemName);
      productData.append('description', this.product.description);
      productData.append('typeGem', this.product.typeGem);
      productData.append('gemPrice', this.product.gemPrice.toString());
      productData.append('unitsAvailable', this.product.unitsAvailable.toString());

      this.productService.createProduct(productData).subscribe((response) => {
        // Maneja la respuesta del servidor (por ejemplo, muestra un mensaje de éxito)
        console.log('Producto creado exitosamente', response);
        this.showCreateNotification(); // Llama a la función con paréntesis

        // Muestra el mensaje de éxito
        this.successMessageVisible = true;

        const productId = response.id;

        // Restablece los valores de los campos del formulario
        this.product.gemName = '';
        this.product.description = '';
        this.product.typeGem = '...';
        this.product.gemPrice = 1; // O el valor inicial que desees
        this.product.unitsAvailable = 1; // O el valor inicial que desees


        setTimeout(() => {
          this.successMessageVisible = false;
        }, 3500);
        // Duración de la notificación en milisegundos
      });
    } else {
      // Maneja el caso en que no se haya seleccionado una imagen o la validación del formulario no pase
      console.error('Debes seleccionar una imagen y completar los campos requeridos.');
      this.submitClicked = true;
    }
  }




updateProduct() {
  if (this.selectedProduct && this.selectedProduct.id) {
    console.log('Datos que se enviarán en el PUT:', this.selectedProduct);
    this.productService.updateProductById(this.selectedProduct.id, this.selectedProduct)
      .subscribe(
        (updatedProduct: Product) => {
          console.log('Producto actualizado:', updatedProduct);
          // Después de la actualización exitosa, recarga la página
          window.location.reload();
        },
        (error) => {
          console.error('Error al actualizar el producto:', error);
          // Maneja el error según tu lógica de la aplicación
        }
      );
  } else {
    console.error('No se ha seleccionado un producto válido para actualizar');
  }
}



  loadProducts() {
    this.productService.getProducts().subscribe((data: Product[]) => {
      this.productList = data;
    });
  }

  onSelectProduct() {
    if (this.selectedProductId) {
      this.productService.getProductById(this.selectedProductId).subscribe((data: Product) => {
        this.selectedProduct = data;
      });
    }
  }

  deleteProduct() {
    if (this.selectedProductIdToDelete) {
      this.productService.deleteProductById(this.selectedProductIdToDelete).subscribe(
        () => {
          console.log('Producto eliminado correctamente');

          // Realiza acciones adicionales si es necesario después de la eliminación

          // Limpia la selección después de eliminar el producto
          this.selectedProductIdToDelete = undefined;
          // Después de eliminar el producto con éxito, recarga la página
          window.location.reload()
        },
        (error) => {
          console.error('Error al eliminar el producto:', error);
          // Maneja el error según tu lógica de la aplicación
        }
      );
    } else {
      console.error('No se ha seleccionado un producto válido para eliminar');
    }
  }

  onSelectProductToDelete() {
       if (this.selectedProductId) {
      this.productService.getProductById(this.selectedProductId).subscribe((data: Product) => {
        this.selectedProduct = data;
      });
  }}


  validateForm(): boolean {
    return !!this.product.gemName;
  }

  showCreateNotification() {
    Swal.fire({
      title: 'Nuevo producto',
      icon: 'success',
      confirmButtonText: 'Creado',
      confirmButtonColor: '#00473A',
      customClass: {
        popup: 'custom-alert'
      },
      timer: 5000, // Muestra la notificación durante 5000 milisegundos (5 segundos)
      timerProgressBar: true, // Muestra una barra de progreso que indica cuánto tiempo queda antes de que se cierre la notificación
      allowOutsideClick: false // Evita que la notificación se cierre cuando se hace clic fuera de ella
    }).then((result) => {
      // Recarga la página cuando se cierra la alerta, ya sea porque se acabó el tiempo o porque el usuario hizo clic en "Ok"
      window.location.reload();
    });
  }


}
