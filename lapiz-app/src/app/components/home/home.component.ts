import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { LoginService } from 'src/app/services/auth/login.service';
import { jwtDecode } from 'jwt-decode';
interface DecodedToken {
  sub: string;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit  {
  username: string | null = null;
  userLoginOn:boolean=false;
  userNameDecoded!: string;
  dropdownOpen = false;
  searchTerm: string = '';
  quantity: number = 0;
  

  constructor(private router: Router, private loginService: LoginService) { }
  ngOnInit(): void {
    this.loginService.currentUserLoginOn.subscribe(
      {
        next:(userLoginOn) => {
          this.userLoginOn=userLoginOn;
        }
      }
    )
    this.loginService.currentUserData.subscribe((username) => {
      this.username = username.toString();
      this.decodedUserName(this.username);
    });
   
  }
  decodedUserName(token: string){
    try {
      const decodedToken = jwtDecode<DecodedToken>(token.toString());
      this.userNameDecoded =  decodedToken.sub;
      console.log("usuario:", decodedToken.sub);
    } catch (error) {
      console.log('Usuario no autenticado');
    }
  }
  
  toggleDropdown() {
    this.dropdownOpen = !this.dropdownOpen;
  }
  loginClicked() {
    console.log('Clic en login');
    this.router.navigate(['login']); // Navega al componente "OtroComponente"
  }

  searchClicked() {
    this.router.navigate(['/products'], { queryParams: { searchTerm: this.searchTerm } });
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['login'])
  }
}

