import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import { Product } from 'src/app/product';
import { ActivatedRoute, Router } from '@angular/router';
import { data } from "autoprefixer";
import { ProductService } from 'src/app/product.service';
import {Observable} from "rxjs";
import {CookieService} from "ngx-cookie-service";
@Component({
  selector: 'app-list-of-products',
  templateUrl: './list-of-products.component.html',
  styleUrls: ['./list-of-products.component.css']
})
export class ListOfProductsComponent implements OnInit {
  name!: string;
  products: Product[] = [];
  inputProduct: string = ''; // Inicializa inputProduct como una cadena vacía
  quantity: number = 0;
  searchTerm: string = '';
  showFilteredProducts: boolean = false; // variable para controlar si se muestran los productos filtrados
  selectedProductId: number | null = null;
  productIds: number[] = [];
  eanInput: any;
  selectedFilter: string = '';
  currentPage = 1;
  pageSize = 10; // Número de productos por página


  constructor(private productService: ProductService, private router: Router, private route: ActivatedRoute, private cookieService: CookieService,private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getAllProductIds();
    this.generateFilterElements(); // Generar elementos de filtro de tipo de producto una vez al principio
    this.obtainProducts();  // Agrega esta línea para obtener los productos al inicio

    // Después de generar los elementos de filtro, puedes llamar a search()
    this.route.queryParams.subscribe((params) => {
      this.searchTerm = params['searchTerm'] || '';
      this.search();
    });
  }
  public search() {
    if (this.searchTerm.trim()) {
      this.productService.filterByKeyword(this.searchTerm).subscribe((data) => {
        this.products = data;
        this.quantity = this.products.length;
      });
    } else {
      this.obtainProducts();
    }
  }

  private generateFilterElements() {
    // Obtener una lista de tipos de productos únicos
    const uniqueType = [...new Set(this.products.map(product => product.typeGem))];
    const filterTypeContainer = document.getElementById('filterType');
    if (filterTypeContainer) {
      filterTypeContainer.innerHTML = '';
      uniqueType.forEach(type => {
        const typeElement = document.createElement('div');
        typeElement.style.margin = '7%';
        typeElement.textContent = type;
        typeElement.addEventListener('click', () => this.filterByType(type));
        filterTypeContainer.appendChild(typeElement);
      });
    }

    const rango1Button = document.getElementById('rango1');
    const rango2Button = document.getElementById('rango2');
    const rango3Button = document.getElementById('rango3');

    if (rango1Button && rango2Button && rango3Button) {
      rango1Button.addEventListener('click', () => this.filterByPrice(100, 150));
      rango2Button.addEventListener('click', () => this.filterByPrice(151, 175));
      rango3Button.addEventListener('click', () => this.filterByPrice(176, 200));
    }
  }

  public filterByType(type: string) {
    if (type && type.trim() !== '') { // comprobamos que name no está vacío
      this.productService.filterByTypeGem(type).subscribe(data => {
        console.log(type);
        this.products = data;
        this.showFilteredProducts = true; // actualizamos el valor de la variable
        this.quantity = this.products.length;
        this.selectedFilter = `Tipo: ${type}`; // Actualiza el filtro actual

        localStorage.setItem('filtroLocalStorage', this.selectedFilter);
        const valor = localStorage.getItem('filtroLocalStorage');
        if (valor) {
          // Usar el valor recuperado en tu aplicación
          console.log('Valor recuperado:', valor);
        }


        console.log('Valor antes de guardar en la cookie:', this.selectedFilter);
        const filtro = { tipo: 'filtro', valor: this.selectedFilter };
        this.cookieService.set('filtroCookie', this.selectedFilter);
        console.log('Valor después de guardar en la cookie:', filtro);

        const filtroGuardado = this.cookieService.get('filtroCookie');
        console.log('Valor después de recuperar de la cookie:', filtroGuardado);
        if (filtroGuardado) {
          console.log('Filtro guardado:', filtroGuardado);
          // Usa el filtro en tu aplicación
        }

      });
    } else {
      // si name está vacío, obtenemos todos los productos
      this.selectedFilter = ''; // Borra el filtro actual
      this.obtainProducts();
    }
  }
  public filterByPrice(minPrice: number, maxPrice: number) {
    if (minPrice >= 0 && maxPrice >= minPrice) {
      // Llamar al servicio para obtener productos filtrados por rango de precio
      this.productService.filterProductsByPriceRange(minPrice, maxPrice).subscribe(data => {
        this.products = data;
        this.showFilteredProducts = true;
        this.quantity = this.products.length;
        this.selectedFilter = `Precio: ${minPrice}-${maxPrice}`; // Actualiza el filtro actual

      });
    } else if (!minPrice && !maxPrice) {
      // Si tanto minPrice como maxPrice están vacíos, obtener todos los productos
      this.obtainProducts();
    } else {
      // Manejar el caso en el que los valores de minPrice y maxPrice no sean válidos
      this.selectedFilter = ''; // Borra el filtro actual
      console.log("Valores de precio no válidos");
    }
  }
  private obtainProducts() {
    this.productService.getProducts().subscribe(data => {
      this.products = data;
      this.showFilteredProducts = false; // aseguramos que se muestren todos los productos
      this.quantity = this.products.length;
      this.generateFilterElements();

    });
  }
  mostrarDiv: boolean = false;
  public toggleDiv() {
    this.mostrarDiv = !this.mostrarDiv;
  }
  selectProduct(productId: number) {
    // Asigna el productId al producto seleccionado
    this.selectedProductId = productId;
    // Navega a la página de detalles del producto
    this.router.navigate(["/details", productId]);
  }

  filterByProductId() {
    if (this.eanInput.trim() !== '') {
      // Convierte el valor de eanInput en un número (asegúrate de que sea un número válido)
      const productId = Number(this.eanInput);

      if (!isNaN(productId)) {
        this.productService.getProductById(productId).subscribe((product) => {
          // Actualiza la lista de productos con el producto filtrado
          this.products = [product];
          this.quantity = this.products.length;
          this.selectedFilter = `EAN: ${productId}`; // Actualiza el filtro actual
        });
      } else {

        console.log('El valor ingresado no es un número válido.');
      }
    } else {
      // Si el campo está vacío, obtener todos los productos
      this.selectedFilter = ''; // Borra el filtro actual
      this.obtainProducts();
    }
  }

    getAllProductIds() {
        this.productService.getAllProductIds().subscribe((ids) => {
            this.productIds = ids;
        });
    }

  clearFilter() {
    this.selectedFilter = ''; // Borra el filtro actual
    this.obtainProducts(); // Vuelve a cargar todos los productos sin filtrar
  }

  searchClicked() {
    // Llama a la función `search()` para realizar la búsqueda utilizando el valor de `searchTerm`
    this.search();
    this.selectedFilter = `Búsqueda: ${this.searchTerm}`;
    this.searchTerm = '';
  }

  CreateClicked() {
    this.router.navigate(['/create']); // Navega al componente "OtroComponente"
  }

  protected readonly Product = Product;
}
