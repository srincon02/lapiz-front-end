import { Component, OnInit, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from "../../cart.service";
import { NgModule } from '@angular/core';
import { Product } from '../../product';
import { ProductService } from 'src/app/product.service';

@Component({
  selector: 'app-cart-product',
  templateUrl: './cart-product.component.html',
  styleUrls: ['./cart-product.component.css']
})

export class CartProductComponent implements OnInit {
  temporaryCartItems: any[] = [];
  selectedPaymentMethod: string = ''; // Variable para rastrear la opción de pago seleccionada
  pagosRealizados: any[] = [];
  comprasRealizadas: any[] = []; // Nuevo array para almacenar las compras realizadas

  // Nuevas variables para acumular costos e impuestos
  totalCost: number = 0;
  totalVat: number = 0;
  grandTotal: number = 0;
  cartItemQuantity: number = 1;

  // Propiedades correspondientes a los campos del formulario
  nombrePropietario: string = '';
  numeroTarjeta: string = '';
  fechaVencimiento: string = '';
  codigoCVV: string = '';
  //Detalles de envío
  name: string = '';
  doc: string = '';
  address: string = '';
  camposBloqueados: boolean = false;
  compraExitosa: boolean = false;
  validacionActiva: boolean = false;
  isCreditCardFormValid: boolean = false;
  metodoPagoHabilitado: boolean = false;
  bancoSeleccionado: string = 'Seleccione';
  correoPSE: string = '';


product!: Product

  constructor(private cartService: CartService, private cd: ChangeDetectorRef, private router: Router, private renderer: Renderer2, private productService: ProductService) { }
  center: google.maps.LatLngLiteral = { lat: 0, lng: 0};
  zoom = 15;
  bandera: boolean = false;
  ngOnInit() {
    console.log('Inicializando el componente del carrito');
    this.temporaryCartItems = this.cartService.getTemporaryCartItems();
    console.log(this.temporaryCartItems);
    this.cd.detectChanges();
    this.updateTotals();
  }

  confirmPurchase() {
    this.temporaryCartItems.forEach(item => {
      this.cartService.addToCart(item);
    });
  }


  decreaseQuantity(item: any) {
    if (item.quantity > 1) {
      item.quantity--;
      this.updateCartItem(item);
    }
    this.updateTotals();
  }

  increaseQuantity(item: any) {
    item.quantity++;
    this.updateCartItem(item);
    this.updateTotals();
  }

  removeFromCart(item: any) {
    const index = this.temporaryCartItems.indexOf(item);
    if (index !== -1) {
      this.temporaryCartItems.splice(index, 1);
      this.cartService.updateTemporaryCart(this.temporaryCartItems);

      // Aquí, actualiza el contador del carrito al modificar el carrito
      this.cartService.updateCartItemCount(this.temporaryCartItems.length);
    }
    this.updateTotals();
  }


  private updateCartItem(item: any) {
    // Realiza cualquier cálculo necesario antes de actualizar el carrito
    item.total = item.quantity * item.product.gemPrice;
    item.vat = 0.19 * item.total;
    item.grandTotal = item.total + item.vat;

    this.cartService.updateTemporaryCart(this.temporaryCartItems);
    this.updateTotals();
  }

  calculateTotalCost(): number {
    return this.temporaryCartItems.reduce((total, item) => {
      return total + (item.product.gemPrice * item.quantity);
    }, 0);
  }

  // Método para actualizar los totales
  updateTotals() {
    this.totalCost = this.calculateTotalCost();
    this.totalCost = this.temporaryCartItems.reduce((acc, item) => acc + item.total, 0);
    this.totalVat = this.temporaryCartItems.reduce((acc, item) => acc + item.vat, 0);
    this.grandTotal = this.totalCost + this.totalVat;
  }

  // Método para manejar el evento cuando se selecciona una opción de pago
  handlePaymentMethodChange(method: string): void {
    this.selectedPaymentMethod = method;
  }

realizarPago(): void {
  if (this.selectedPaymentMethod === 'tarjeta' ) {
    if (this.validarTarjetaCredito()) {
      const nuevaCompra = {
        doc: this.doc,
        name: this.name,
        addresst: this.address,
        vat: this.totalVat,
        price: this.totalCost,
        total: this.grandTotal,
        datepruchase: new Date().toISOString()
      };

    this.cartService.guardarCompra(nuevaCompra)
      .subscribe(
        (response) => {
          console.log('Datos de compra enviados al backend con éxito:', response);
          // Realizar cualquier acción adicional después de enviar los datos al backend
          // Obtener el ID de la transacción de la respuesta del backend
          const idPurchase = response.idpurchase;

          // Enviar los detalles de la compra uno por uno al backend
          this.enviarDetallesCompra(idPurchase);

// Lógica después de la compra...
        this.cartService.clearTemporaryCart(); // Limpiar el carrito

        this.compraExitosa = true; // Cambiar a true para mostrar la notificación

        // Recargar la página después de un pequeño retraso (por ejemplo, 2 segundos)
        setTimeout(() => {
          // Redirigir al componente 'products' con el parámetro 'searchTerm'
          this.router.navigate(['/products']).then(() => {
          this.renderer.setProperty(document.documentElement, 'scrollTop', 0);
          });
        }, 2000); // 2000 milisegundos = 2 segundos
      },
        (error) => {
          console.error('Error al enviar datos de compra al backend:', error);
          // Manejar errores si es necesario
        }
      );
   } else {
      console.log('Por favor, verifica los campos del formulario de tarjeta de crédito.');
    }
  }  else if (this.selectedPaymentMethod === 'pse') {
    if (this.validarFormularioPSE(this.bancoSeleccionado, this.correoPSE)) {
      const nuevaCompraPSE = {
        doc: this.doc,
        name: this.name,
        addresst: this.address,
        vat: this.totalVat,
        price: this.totalCost,
        total: this.grandTotal,
        datepruchase: new Date().toISOString()
      };
      this.cartService.guardarCompra(nuevaCompraPSE)
      .subscribe(
        (response) => {
          console.log('Datos de compra enviados al backend con éxito:', response);
          // Realizar cualquier acción adicional después de enviar los datos al backend
          // Obtener el ID de la transacción de la respuesta del backend
          const idPurchase = response.idpurchase;

          // Enviar los detalles de la compra uno por uno al backend
          this.enviarDetallesCompra(idPurchase);

// Lógica después de la compra...
        this.cartService.clearTemporaryCart(); // Limpiar el carrito

        this.compraExitosa = true; // Cambiar a true para mostrar la notificación

        // Recargar la página después de un pequeño retraso (por ejemplo, 2 segundos)
        setTimeout(() => {
          // Redirigir al componente 'products' con el parámetro 'searchTerm'
          this.router.navigate(['/products']).then(() => {
          this.renderer.setProperty(document.documentElement, 'scrollTop', 0);
          });
        }, 2000); // 2000 milisegundos = 2 segundos
      },
        (error) => {
          console.error('Error al enviar datos de compra al backend:', error);
          // Manejar errores si es necesario
        }
      );

      // Lógica para realizar el pago mediante PSE
      console.log('Pago mediante PSE procesado...');
    } else {
      console.log('Por favor, verifica los campos del formulario de PSE.');
    }
  } else {
    console.log('Seleccione un método de pago válido.');
  }
}

async enviarDetallesCompra(idPurchase: number): Promise<void> {
  for (const item of this.temporaryCartItems) {
    const detalleCompra = {
      id_gema: item.productId,
      cantidad: item.quantity,
      id_compra: idPurchase // ID de la transacción principal
    };

    try {
      const detallesResponse = await this.cartService.guardarDetallesCompra(detalleCompra).toPromise();
      console.log('Detalle de compra enviado al backend con éxito:', detallesResponse);

    } catch (detallesError) {
      console.error('Error al enviar detalle de compra al backend:', detallesError);
      // Manejar errores si es necesario
    }
  }
}



  validarFormularioTarjeta(): boolean {
  if (this.selectedPaymentMethod !== 'tarjeta') {
    return true; // Si no se selecciona tarjeta de crédito, se considera válido
  }
    const nombreValido: boolean = this.nombrePropietario.trim() !== '';
    const numeroTarjetaValido: boolean = /^\d{12}$/.test(this.numeroTarjeta);
    const fechaValida: boolean = /^(0[1-9]|1[0-2])\/\d{4}$/.test(this.fechaVencimiento);
    const codigoCVVValido: boolean = /^\d{3}$/.test(this.codigoCVV);
    return nombreValido && numeroTarjetaValido && fechaValida && codigoCVVValido;
  }

  validarYConfirmar(): void {
  if (this.validacionActiva) {
    this.confirmar_all(); // Realizar la validación solo si la variable de validación está activa
  } else {
    this.camposBloqueados = true; // Si no se realiza la validación, bloquear los campos
  }
}


  initMap() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const lat = position.coords.latitude;
        const lng = position.coords.longitude;
        this.center = { lat: lat, lng: lng };
        this.address = this.center.lat + "," + this.center.lng
        this.zoom = 15;
        this.bandera = !this.bandera;
      });
    } else {
      this.address = this.address;
    }
  }

  validarNombre(): boolean {
    const regexNombre = /^[A-Za-zÁÉÍÓÚáéíóúñÑ\s]+$/; // Expresión regular para validar letras y espacios
    return regexNombre.test(this.name);
  }

  validarDocumento(): boolean {
    const regexDocumento = /^\d+$/; // Expresión regular para validar números
    return regexDocumento.test(this.doc);
  }

  confirmar_all(): void {
    if (!this.validarNombre()) {
      // Mostrar mensaje de error para nombre inválido
      console.log('El campo de nombre solo debe contener letras y espacios.');
      this.verificarDatosEnvio();
      return;
    }

    if (!this.validarDocumento()) {
      // Mostrar mensaje de error para documento inválido
      console.log('El campo de documento de identidad solo debe contener números.');
      return;
    }

    // Resto de la lógica cuando la validación es exitosa
    //this.camposBloqueados = true;
  }

    limpiarCampos(): void {
    this.name = '';
    this.doc = '';
    this.address = '';
    this.camposBloqueados = false;
    this.metodoPagoHabilitado = false;
    this.initMap();
  }

    verificarDatosEnvio() {
    // Verificar si los campos de datos de envío están llenos
    if (this.name.trim() !== '' && this.doc.trim() !== '' && this.address.trim() !== '') {
      // Si los campos de datos de envío están completos, habilitar la selección de método de pago
      this.metodoPagoHabilitado = true;
    } else {
      // Si algún campo de datos de envío está vacío, deshabilitar la selección de método de pago
      this.metodoPagoHabilitado = false;
    }
  }

  verificarCamposDiligenciados(): boolean {
  return this.validarNombre() && this.validarDocumento();
}


validarNombrePropietario(): boolean {
  return /^[A-Za-zÁÉÍÓÚáéíóúñÑ\s]+$/.test(this.nombrePropietario);
}

validarNumeroTarjeta(): boolean {
  return /^\d{12}$/.test(this.numeroTarjeta);
}

validarFechaVencimiento(): boolean {
  return /^(0[1-9]|1[0-2])\/\d{4}$/.test(this.fechaVencimiento);

  if (!this.fechaVencimiento) {
    return false; // Si la fecha no está ingresada, es inválida
  }

  // Obtener el mes y el año de la fecha actual
  const fechaActual = new Date();
  const añoActual = fechaActual.getFullYear();
  const mesActual = fechaActual.getMonth() + 1; // Sumar 1 porque en JavaScript los meses van de 0 a 11

  // Obtener el mes y el año de la fecha ingresada
  const [mes, año] = this.fechaVencimiento.split('/');
  const añoIngresado = Number(año);
  const mesIngresado = Number(mes);

  // Verificar si el año es mayor o si es el mismo año pero el mes es mayor o igual al mes actual
  return añoIngresado > añoActual || (añoIngresado === añoActual && mesIngresado >= mesActual);
}

validarCodigoCVV(): boolean {
  return /^\d{3}$/.test(this.codigoCVV);
}

validarTarjetaCredito(): boolean {
  return (
    this.validarNombrePropietario() &&
    this.validarNumeroTarjeta() &&
    this.validarFechaVencimiento() &&
    this.validarCodigoCVV()
  );
  }

  validarSeleccion(bancoSeleccionado: string): boolean {
    return bancoSeleccionado !== 'Seleccione';
  }

  validarCorreoPSE(correoPSE: string): boolean {
    // Utilizamos una expresión regular para validar que el campo sea un correo electrónico válido
    const regexCorreo = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regexCorreo.test(correoPSE);
  }

  validarFormularioPSE(bancoSeleccionado: string, correoPSE: string): boolean {
    const seleccionValida = this.validarSeleccion(bancoSeleccionado);
    const correoValido = this.validarCorreoPSE(correoPSE);

    return seleccionValida && correoValido;
  }

  realizarPagoPSE(): void {
  const formularioPSEValido = this.validarFormularioPSE(this.bancoSeleccionado, this.correoPSE);

  if (formularioPSEValido) {
    // Realizar la lógica para el pago mediante PSE
    console.log('El formulario de PSE es válido. Procesar el pago...');
    // Agregar aquí la lógica para procesar el pago con PSE
  } else {
    console.log('Por favor, verifica los campos del formulario de PSE.');
  }
}

}