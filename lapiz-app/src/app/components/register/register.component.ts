import { Component } from '@angular/core';
import {UserService} from "../../user.service";
import {User} from "../../user";
import {ActivatedRoute, Router} from "@angular/router";
import Swal from "sweetalert2";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user: User = new User();
  passwordFieldType = 'password';
  submitClicked = false;

  constructor(private userService: UserService, private router:Router) {}



  registerUser(): void {
    this.submitClicked = true;

    if (this.validateForm()) {
      this.userService.registerUser(this.user).subscribe(
        (response) => {
          console.log('Usuario registrado:', response);
          this.showCommentNotification(); // Muestra notificación de registro exitoso
          this.clearForm();
          // Puedes realizar acciones adicionales después de un registro exitoso si es necesario.
        },
        (error) => {
          console.error('Error al registrar usuario:', error);

          if (error.status === 502) {
            // 409 indica conflicto, en este caso, el correo ya existe
            this.showExistingUserNotification();
          } else {
            // Otros errores, manejar según sea necesario
            // Puedes mostrar un mensaje genérico de error o realizar acciones específicas.
          }

          this.submitClicked = false; // Restablece el estado del botón
        }
      );
    }
  }

  togglePasswordVisibility() {
    this.passwordFieldType = this.passwordFieldType === 'password' ? 'text' : 'password';
  }

  validateForm(): "" | 0 | String {
    // Verificar si los campos obligatorios están llenos
    return (
      this.user.username &&
      this.user.fullName &&
      this.user.userNumberPhone &&
      this.user.password
    );
  }

  LoginClicked(): void {
    this.router.navigate(['/login']);
    // Aquí puedes realizar las acciones necesarias cuando se hace clic en "Inicia Sesión".
  }

  showCommentNotification() {
    Swal.fire({
      title: 'Nuevo usuario',
      icon: 'success',
      confirmButtonText: 'Registrado',
      confirmButtonColor: '#00473A',
      customClass: {
        popup: 'custom-alert'
      },
      timer: 5000, // Muestra la notificación durante 5000 milisegundos (5 segundos)
      timerProgressBar: true, // Muestra una barra de progreso que indica cuánto tiempo queda antes de que se cierre la notificación
      allowOutsideClick: false // Evita que la notificación se cierre cuando se hace clic fuera de ella
    }).then((result) => {
      // Recarga la página cuando se cierra la alerta, ya sea porque se acabó el tiempo o porque el usuario hizo clic en "Ok"
      window.location.reload();
    });
  }

  showExistingUserNotification() {
    Swal.fire({
      title: 'Error de Registro',
      text: 'El correo electrónico ya está registrado. Inicia sesión o utiliza otro correo.',
      icon: 'error',
      confirmButtonText: 'Ok',
      confirmButtonColor: '#FF0000',
      customClass: {
        popup: 'custom-alert'
      },
      timer: 5000,
      timerProgressBar: true,
      allowOutsideClick: false
    });
  }

  clearForm() {
    this.user = new User();
    this.passwordFieldType = 'password';
    this.submitClicked = false;
  }

}
