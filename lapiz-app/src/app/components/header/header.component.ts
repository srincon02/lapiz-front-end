import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router  } from "@angular/router";
import { LoginService } from 'src/app/services/auth/login.service';
import { jwtDecode } from 'jwt-decode';
import { CartService } from 'src/app/cart.service';
interface DecodedToken {
  sub: string;
}
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username: string | null = null;
  userLoginOn: boolean = false;
  userNameDecoded!: string;
  dropdownOpen = false;
  cartItemCount: number = 0;
  aboutInfoOpen = false;

  constructor(private router: Router, private location: Location, private loginService: LoginService, private cartService: CartService) { }
  ngOnInit(): void {
    this.cartService.currentCartItemCount.subscribe(count => {
      this.cartItemCount = count;
    });
    this.loginService.currentUserLoginOn.subscribe(
      {
        next: (userLoginOn) => {
          this.userLoginOn = userLoginOn;
        }
      }
    )
    this.loginService.currentUserData.subscribe((username) => {
      this.username = username.toString();
      this.decodedUserName(this.username);
    });


  }
  decodedUserName(token: string) {
    try {
      const decodedToken = jwtDecode<DecodedToken>(token.toString());
      this.userNameDecoded = decodedToken.sub;
      console.log("usuario:", decodedToken.sub);
    } catch (error) {
      console.log('Usuario no autenticado');
    }
  }
  toggleDropdown() {
    this.dropdownOpen = !this.dropdownOpen;
  }
  backClicked() {
    this.location.back();
  }
  loginClicked() {
    this.router.navigate(['/login']); // Navega al componente "OtroComponente";
  }
  logout() {
    this.loginService.logout();
    this.router.navigate(['login'])
  }
  navigateToCart() {
    this.router.navigate(['/cart']);
  }

  titleHome() {
    this.router.navigate(['/home']); // Navega al componente
  }

  toggleAboutInfo(): void {
    this.aboutInfoOpen = !this.aboutInfoOpen;
  }

}
