import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Comment} from "./comment";

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private baseUrl = "http://localhost:8080";// usar el puerto 5000"http://localhost:5000";

  constructor(private http: HttpClient) { }

  // Método para obtener una revisión por su ID
  getReviewById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/reviews/${id}`);
  }

  // Método para crear una revisión
  createReview(review: Comment): Observable<Object> {
    return this.http.post(`${this.baseUrl}/createReview`, review); // URL correcta
    console.log(review)
  }

  getReviewsByGemCode(gemCode: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.baseUrl}/reviews/gemCode/${gemCode}`);
  }

}
